package ocean

import (
	"testing"
)

func TestRegions(t *testing.T) {
	regions, err := client.Regions()
	if err != nil {
		t.Fatal(err)
	}
	rfound := false
	rslug := "lon1"
	for _, v := range regions {
		if rslug == v.Slug {
			rfound = true
		}
	}
	if len(regions) < 6 {
		t.Fatalf("there should be more than 6 regions returned got %d", len(regions))
	}
	if !rfound {
		t.Fatalf("expected to find the %s region", rslug)
	}
}
