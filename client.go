package ocean

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	ErrBadPagination     = errors.New("pagination links in response would cause recursion")
	ErrBadResponse       = errors.New("no response was received")
	ErrBadResponseBody   = errors.New("failed to read api response")
	ErrBadResponseData   = errors.New("failed to parse api response")
	ErrBadResponseStatus = errors.New("unexpected response status")
	ErrNotFound          = errors.New("record could not be found")
)

// A "resty thing" is something that has a REST path.
type resty interface {
	path() string
}

// Something that can init
type initer interface {
	init(c *Client) error
}

// pages contain infomation about how to fetch other result "pages".
type pages struct {
	First string `json:"first,omitempty"`
	Prev  string `json:"prev,omitempty"`
	Next  string `json:"next,omitempty"`
	Last  string `json:"last,omitempty"`
}

// links contain information about other resources released to this one.
type links struct {
	Pages pages `json:"pages"`
}

// meta contains additional data about a given response.
type meta struct {
	Total int64 `json:"total"`
}

// response is the internal response object containing fields for all possible responses.
type response struct {
	// Response info.
	Meta meta
	// Pagination data.
	Links links
	// Account response data.
	Account *Account `json:"account"`
	// Actions response data.
	Action  *Action   `json:"action"`
	Actions []*Action `json:"actions"`
	// Domains response data.
	Domain  *Domain         `json:"domain"`
	Domains []*Domain       `json:"domains"`
	Record  *DomainRecord   `json:"domain_record"`
	Records []*DomainRecord `json:"domain_records"`
	// Droplets
	Droplet  *Droplet   `json:"droplet"`
	Droplets []*Droplet `json:"droplets"`
	// Kernels
	Kernel  *Kernel   `json:"kernel"`
	Kernels []*Kernel `json:"kernels"`
	// Snapshots
	Snapshot  *Snapshot   `json:"snapshot"`
	Snapshots []*Snapshot `json:"snapshots"`
	// Backups
	Backup  *Backup   `json:"backup"`
	Backups []*Backup `json:"backups"`
	// Regions
	Region  *Region   `json:"region"`
	Regions []*Region `json:"regions"`
	// Images
	Image  *Image   `json:"image"`
	Images []*Image `json:"images"`
	// Images
	Size  *Size   `json:"size"`
	Sizes []*Size `json:"sizes"`
	// Keys
	Key  *Key   `json:"ssh_key"`
	Keys []*Key `json:"ssh_keys"`
	// errors
	err error
}

// takes a reflect Value that must be a ptr
// attempts to call init on it.
func tryToInit(v interface{}, c *Client) (err error) {
	record, ok := v.(initer)
	if ok {
		err = record.init(c)
	}
	return
}

// init calls init() on any *Things that implement initer in the reponse
func (resp *response) init(c *Client) error {
	rv := reflect.ValueOf(resp).Elem()
	rt := rv.Type()
	for i := 0; i < rt.NumField(); i++ {
		ft := rt.Field(i)
		if ft.Tag.Get("json") == "" {
			continue
		}
		fv := rv.Field(i)
		switch ft.Type.Kind() {
		case reflect.Slice:
			for j := 0; j < fv.Len(); j++ {
				if err := tryToInit(fv.Index(j).Interface(), c); err != nil {
					return err
				}
			}
		case reflect.Ptr:
			if fv.IsNil() {
				continue
			}
			if err := tryToInit(fv.Interface(), c); err != nil {
				return err
			}
		}
	}
	return nil
}

type errorResponse struct {
	Id          string `json:"id"`
	Message     string `json:"message"`
	Description string `json:"description"`
}

// errorResponse
func (err *errorResponse) Error() string {
	return err.Message
}

// Interface for client logger
type Logger interface {
	Println(v ...interface{})
}

//
type Client struct {
	// Your Digital Ocean API Key
	ApiKey string
	// Limit of items per page
	PerPage int
	// Enable nosiy HTTP traffic logging
	Logger Logger
	// Http client used. Override if you need special config
	HttpClient *http.Client
	// If set to true the client will cache things that don't change often (like regions & sizes).
	EnableCache bool
	// Time between requests when polling for action state
	PollDuration time.Duration
	// The number of requests that can be made per hour.
	requestLimit int64
	// The number of requests that remain before you hit your request limit.
	requestsRemaining int64
	// This represents the time when the oldest request will expire.
	requestLimitReset time.Time
	// Account cache
	cachedAccount *Account
	accountLock   sync.Mutex
	// Region cache
	cachedRegions []*Region
	regionsLock   sync.Mutex
	// Size cache
	cachedSizes []*Size
	sizesLock   sync.Mutex
	// Image cache
	cachedImages []*Image
	imagesLock   sync.Mutex
}

// Create a client object for interacting
func NewClient(key string) (c *Client) {
	c = &Client{
		ApiKey:            key,
		HttpClient:        &http.Client{},
		PerPage:           25,
		EnableCache:       true,
		PollDuration:      time.Second * 4,
		requestLimit:      -1,
		requestsRemaining: -1,
	}
	return c
}

// Reset clears any cached records.
// If client.EnableCache is true (which it is by default) the client will
// cache certain records that don't change often. Reset will clear these caches.
func (c *Client) Reset() {
	c.cachedAccount = nil
	c.cachedRegions = nil
	c.cachedSizes = nil

}

// The number of requests that remain before you hit your request limit.
// If the client has not made any requests yet calling this method will trigger
// an API call to fetch the required data.
func (c *Client) RequestLimit() (n int64, err error) {
	if c.requestLimit == -1 {
		_, err = c.Account()
	}
	return c.requestLimit, err
}

// The number of requests that remain before you hit your request limit.
// If the client has not made any requests yet calling this method will trigger
// an API call to fetch the required data.
func (c *Client) RequestsRemaining() (n int64, err error) {
	if c.requestLimit == -1 {
		_, err = c.Account()
	}
	return c.requestsRemaining, err
}

// This represents the time when the oldest request will expire.
// If the client has not made any requests yet calling this method will trigger
// an API call to fetch the required data.
func (c *Client) RequestLimitResetAt() (t time.Time, err error) {
	if c.requestLimitReset.IsZero() {
		_, err = c.Account()
	}
	return c.requestLimitReset, err
}

// Log using Logger if set
func (c *Client) log(v ...interface{}) {
	if c.Logger != nil {
		c.Logger.Println(v...)
	}
}

// rawish http request
func (c *Client) request(method string, path string, data interface{}) (resp *response, err error) {
	resp = &response{}
	// Build URI
	if path == "" {
		panic("no object path given")
	}
	uri := fmt.Sprintf("https://api.digitalocean.com/v2/%s", path)
	// Encode body if needed.
	var r io.Reader
	var b []byte
	if data != nil && (method == "POST" || method == "PUT") {
		b, err = json.Marshal(data)
		if err != nil {
			return
		}
		r = bytes.NewReader(b)
	}
	// Make HTTP request.
	c.log("request:", method, path, string(b))
	req, err := http.NewRequest(method, uri, r)
	if err != nil {
		return
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.ApiKey))
	req.Header.Add("Content-Type", "application/json")
	res, err := c.HttpClient.Do(req)
	if err != nil {
		return
	}
	c.log("response:", res)
	// Parse limit headers (skipping error handling as not having these is not the end of the world).
	c.requestLimit, _ = strconv.ParseInt(res.Header.Get("Ratelimit-Limit"), 10, 64)
	c.requestsRemaining, _ = strconv.ParseInt(res.Header.Get("Ratelimit-Remaining"), 10, 64)
	timestamp, _ := strconv.ParseInt(res.Header.Get("Ratelimit-Reset"), 10, 64)
	if err == nil {
		c.requestLimitReset = time.Unix(timestamp, 0)
	}
	// Read body.
	b, err = ioutil.ReadAll(res.Body)
	if err != nil {
		err = ErrBadResponseBody
		return
	}
	c.log("response body:", string(b))
	// Handle error response.
	if res.StatusCode >= 400 {
		var e errorResponse
		err = json.Unmarshal(b, &e)
		if err != nil {
			err = ErrBadResponseData
			return
		}
		err = &e
		if err.Error() == "" {
			err = fmt.Errorf(res.Status)
		}
		return
	}
	// Handle unexpected response codes.
	if res.StatusCode >= 300 || res.StatusCode < 200 {
		c.log("unexpected response status: " + res.Status)
		err = ErrBadResponseStatus
		return
	}
	// Parse response body.
	if res.StatusCode != 204 {
		err = json.Unmarshal(b, resp)
		if err != nil {
			return
		}
	}
	// init
	err = resp.init(c)
	if err != nil {
		return
	}
	return
}

// request makes an api request via HTTP.
// request may make multiple HTTP requests if multiple pages of results exist, but only one "response" will be
// returned.
func (c *Client) requestAll(method string, path string, data interface{}, filters ...string) (resp *response, err error) {
	resp = &response{}
	pages := make(chan *response, 64)
	go func() {
		filters = append(filters, fmt.Sprintf("per_page=%d", c.PerPage))
		opts := strings.Join(filters, "&")
		for {
			page, e := c.request("GET", fmt.Sprintf("%s?%s", path, opts), nil)
			if e != nil {
				err = e
				break
			}
			pages <- page
			if page.Links.Pages.Next == "" {
				break
			}
			uri, e := url.Parse(page.Links.Pages.Next)
			if e != nil {
				err = e
				break
			}
			if opts == uri.RawQuery {
				err = fmt.Errorf("pagination error: next link same as current link")
				break
			}
			opts = uri.RawQuery
		}
		pages <- nil
	}()
	// merge all pages into one big response
	for page := range pages {
		if page == nil {
			close(pages)
			break
		}
		err = resp.merge(page)
		if err != nil {
			return
		}
	}
	if err != nil {
		return
	}
	// call init on each initer
	err = resp.init(c)
	if err != nil {
		return
	}
	return resp, err
}

// GET requests
func (c *Client) get(path string, filters ...string) (*response, error) {
	return c.requestAll("GET", path, nil, filters...)
}

// POST request
func (c *Client) post(path string, data interface{}) (*response, error) {
	return c.request("POST", path, data)
}

// PUT requests
func (c *Client) put(path string, data interface{}) (*response, error) {
	return c.request("PUT", path, data)
}

// DELETE requests
func (c *Client) del(path string) (err error) {
	_, err = c.request("DELETE", path, nil)
	return
}

// like path.Join but converts all args to strings
func join(args ...interface{}) string {
	ss := make([]string, len(args))
	for i := 0; i < len(args); i++ {
		ss[i] = fmt.Sprintf("%v", args[i])
	}
	return path.Join(ss...)
}
