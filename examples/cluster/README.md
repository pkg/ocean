# Cluster

This is an example cli tool for spinning up a CoreOS cluster on digital ocean 
using the [ocean](bitbucket.org/pkg/ocean) client library.

## Build and Run 

```bash
❯ GOPATH=$PWD go get cluster
❯ GOPATH=$PWD go install cluster
❯ ./bin/cluster -h
Usage:
  cluster [OPTIONS]

Application Options:
      --api-key=       A valid Digital Ocean API key string ($DOKEY)
      --ssh-key=       Path to an ssh public key to use for the cluster ($HOME/.ssh/id_rsa.pub)
      --instance-size= The size of each droplet (2gb)
      --cluster-size=  Number of machines in the cluster (3)
      --region=        Which region to create droplets in (lon1)

Help Options:
  -h, --help           Show this help message
```


