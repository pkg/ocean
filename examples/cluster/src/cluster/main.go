// cluster is cli tool for spinning up a coreos cluster
// on Digial Ocean.
//
// It is written as an example of using the `ocean` client
// libary: bitbucket.org/pkg/ocean
package main

import(
	"errors"
	"os"
	"os/signal"
	"fmt"
	"io/ioutil"
	"github.com/jessevdk/go-flags"
	"bitbucket.org/pkg/ocean"
)

const(
	CoreOsSlug = "coreos-alpha"
	KeyName = "corekey"
)

// Command line options
type Cli struct {
	// A valid digital ocean API key
	ApiKey string `long:"api-key" default:"$DOKEY" description:"A valid Digital Ocean API key string"`
	// Path to your public ssh key
	SshKey string `long:"ssh-key" default:"$HOME/.ssh/id_rsa.pub" description:"Path to an ssh public key to use for the cluster"`
	// Size of machine
	InstanceSize string `long:"instance-size" default:"2gb" description:"The size of each droplet"`
	// Number of machines in cluster
	ClusterSize int `long:"cluster-size" default:"3" description:"Number of machines in the cluster"`
	// Where to put the cluster
	Region string `long:"region" default:"lon1" description:"Which region to create droplets in"`
	// The client
	client *ocean.Client
}

// Entrypoint for the cli execution
func (cmd *Cli) Execute(args []string) (err error) {
	// we'll need to know if we completed ok so 
	// we can destroy machines if we fail
	success := false

	// We need an API key...
	cmd.ApiKey = os.ExpandEnv(cmd.ApiKey)
	if cmd.ApiKey == "" {
		return e("you must specify a digital ocean api key either via --api-key or using the environment variable DOKEY")
	}
	cmd.client = ocean.NewClient(cmd.ApiKey)

	// We might want to upload a new SSH key...
	if cmd.SshKey == "" {
		cmd.SshKey = os.ExpandEnv(cmd.SshKey)
		sshKeyData, err := ioutil.ReadFile(cmd.SshKey)
		if err != nil {
			return e("could not use ssh key at", cmd.SshKey, ":", err, "use --ssh-key to set path to key")
		}
		if len(sshKeyData) == 0 {
			return e("could not use ssh key at", cmd.SshKey, ":", "file was empty")
		}
		_, err = cmd.client.Key(KeyName)
		if err == nil {
			_, err = cmd.client.NewKey(KeyName, string(sshKeyData))
			if err != nil {
				return err
			}
		}
	}

	// We'll assign all our keys to the cluster
	keys, err := cmd.client.Keys()
	if err != nil {
		return
	}
	if len(keys) == 0 {
		return e("you need to specify an ssh-key via --ssh-key")
	}

	// We need to know the region
	if cmd.Region == "" {
		return e("you must specify a region slug via --region")
	}
	region,err := cmd.client.Region(cmd.Region)
	if err != nil {
		return
	}

	// We need to know the size of each machine
	if cmd.InstanceSize == "" {
		return e("you must specify a size of the droplets via --instance-size")
	}
	size, err := cmd.client.Size(cmd.InstanceSize)
	if err != nil {
		return
	}

	// Get coreos image
	coreos,err := cmd.client.Image(CoreOsSlug)
	if err != nil {
		return
	}

	// We need cluster config
	cfg, err := NewConfig()
	if err != nil {
		return
	}

	// We need to know how many machines
	if cmd.ClusterSize < 3 {
		return e("cluster must consist of at least 3 machines")
	}

	// Create a new droplets.
	machines := make([]*ocean.Droplet, cmd.ClusterSize)
	for i,_ := range machines {
		machines[i], err = cmd.client.NewDroplet(&ocean.DropletConfig{
			Name:              fmt.Sprintf("coreos-node-%d", i),
			Region:            region,
			Size:              size,
			Image:             coreos,
			Keys:              keys,
			Backups:           false,
			PrivateNetworking: true,
			UserData:          cfg,
			Ipv6:              true,
		})
		if err != nil {
			return err
		}
		defer func(i int){
			if !success { // should we rollback?
				fmt.Println("attempting to destroy droplet... you might want to check it's really gone!")
				machines[i].Destroy()
				return
			}
		}(i)
	}

	// wait for machines to finish 
	for _,d := range machines {
		fmt.Printf("waiting for %s to spin up...")
		a,err := d.PendingAction()
		if err != nil {
			fmt.Printf("FAIL\n")
			return err
		}
		if a != nil {
			a.Wait()
		}
		fmt.Printf("ok\n")
	}

	// Done!
	success = true
	machines, err = cmd.client.Droplets()
	if err != nil {
		return e("could not show status of machines ... but pretty sure they are running:", err)
	}
	fmt.Println("Started your CoreOS cluster:")
	for i,d := range machines {
		for _,n := range machines[i].Networks.V4 {
			fmt.Println(d.Name, ":", n.IpAddress)
		}
	}

	return nil
}


// ..because lazy
func e(args ...interface{}) error {
	return errors.New(fmt.Sprintln(args...))
}

// Parse and run the command
func main(){
	// Catch SIGINT unless you really mean it!
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		fmt.Println("Prevented user interupt as you might leave yourself with a broken cluster. Hit Ctrl+C a couple more times if you really mean it")
		<-c
		<-c
	}()
	// Parse CLI, execute it and dump any errors.
	var cmd Cli
	p := flags.NewParser(&cmd, flags.HelpFlag|flags.PassDoubleDash)
	args, err := p.Parse()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		os.Exit(1)
	}
	err = cmd.Execute(args)
	if err != nil {
		fmt.Println("cluster creation failed")
		fmt.Fprintf(os.Stderr, "%s\n", err.Error())
		os.Exit(1)
	}
}
