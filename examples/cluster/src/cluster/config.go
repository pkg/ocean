package main

import(
	"text/template"
	"bytes"
	"net/http"
	"io/ioutil"
)


// Generate a new etcd discovery URL
func newDiscoveryUrl() (url string) {
	res, err := http.Get("https://discovery.etcd.io/new")
	if err != nil {
		return ""
	}
	defer res.Body.Close()
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return ""
	}
	return string(b)
}

func NewConfig() (cfg string, err error) {
	data := struct{
		DiscoveryUrl string
	}{
		DiscoveryUrl: newDiscoveryUrl(),
	}
	if data.DiscoveryUrl == "" {
		err = e("failed to generate an etcd discovery url.... try again later")
		return
	}
	buf := bytes.NewBufferString("")
	err = config.Execute(buf, data)
	if err != nil {
		return
	}
	return buf.String(), nil
}

var config = template.Must(template.New("config").Parse(`#cloud-config
---
coreos:
  etcd:
    discovery: {{ .DiscoveryUrl }}
    addr: $private_ipv4:4001
    peer-addr: $private_ipv4:7001
    # give etcd more time if it's under heavy load - prevent leader election thrashing
    peer-election-timeout: 2000
    # heartbeat interval should ideally be 1/4 or 1/5 of peer election timeout
    peer-heartbeat-interval: 500
  fleet:
    public-ip: $private_ipv4
    etcd_request_timeout: 3
  units:
  - name: etcd.service
    command: start
  - name: fleet.service
    command: start
  - name: fleet.socket
    command: start
    content: |
      [Socket]
      ListenStream=/var/run/fleet.sock
      Service=fleet.service

      [Install]
      WantedBy=sockets.target
  - name: stop-update-engine.service
    command: start
    content: |
      [Unit]
      Description=stop update-engine

      [Service]
      Type=oneshot
      ExecStart=/usr/bin/systemctl stop update-engine.service
      ExecStartPost=/usr/bin/systemctl mask update-engine.service
  - name: install-deisctl.service
    command: start
    content: |
      [Unit]
      Description=Install deisctl utility

      [Service]
      Type=oneshot
      ExecStart=/usr/bin/sh -c 'curl -sSL --retry 5 --retry-delay 2 http://deis.io/deisctl/install.sh | sh -s 1.0.0'
write_files:
  - path: /etc/deis-release
    content: |
      DEIS_RELEASE=v1.0.0
  - path: /etc/motd
    content: ""
  - path: /etc/profile.d/nse-function.sh
    permissions: '0755'
    content: |
      function nse() {
        docker exec -it $1 bash
      }
  - path: /run/deis/bin/get_image
    permissions: '0755'
    content: |
      #!/bin/bash
      # usage: get_image <component_path>
      IMAGE=$(etcdctl get $1/image 2>/dev/null)

      # if no image was set in etcd, we use the default plus the release string
      if [ $? -ne 0 ]; then
        RELEASE=$(etcdctl get /deis/platform/version 2>/dev/null)

        # if no release was set in etcd, use the default provisioned with the server
        if [ $? -ne 0 ]; then
          source /etc/deis-release
          RELEASE=$DEIS_RELEASE
        fi

        IMAGE=$1:$RELEASE
      fi

      # remove leading slash
      echo ${IMAGE#/}
  - path: /opt/bin/deis-debug-logs
    permissions: '0755'
    content: |
      #!/bin/bash

      echo '--- VERSIONS ---'
      source /etc/os-release
      echo $PRETTY_NAME
      source /etc/deis-release
      echo "Deis $DEIS_RELEASE"
      etcd -version
      fleet -version
      printf "\n"

      echo '--- SYSTEM STATUS ---'
      journalctl -n 50 -u etcd --no-pager
      journalctl -n 50 -u fleet --no-pager
      printf "\n"

      echo '--- DEIS STATUS ---'
      deisctl list
      etcdctl ls --recursive /deis
      printf "\n"
  - path: /home/core/.toolboxrc
    owner: core
    content: |
      TOOLBOX_DOCKER_IMAGE=ubuntu-debootstrap
      TOOLBOX_DOCKER_TAG=14.04
      TOOLBOX_USER=root

`))

