package ocean

type DomainConfig struct {
	// The type of the DNS record (ex: A, CNAME, TXT, ...).
	Type string `json:"type"`
	// The name to use for the DNS record.
	Name string `json:"name"`
	// The value to use for the DNS record.
	Data string `json:"data"`
	// The priority for SRV and MX records.
	Priority *int64 `json:"priority"`
	// The port for SRV records.
	Port *int64 `json:"port"`
	// The weight for SRV records.
	Weight *int64 `json:"weight"`
}

// DNS records
type DomainRecord struct {
	// A unique identifier for each domain record.
	Id int64 `json:"id" pk:"true"`
	// The type of the DNS record (ex: A, CNAME, TXT, ...).
	Type string `json:"type"`
	// The name to use for the DNS record.
	Name string `json:"name"`
	// The value to use for the DNS record.
	Data string `json:"data"`
	// The priority for SRV and MX records.
	Priority *int64 `json:"priority"`
	// The port for SRV records.
	Port *int64 `json:"port"`
	// The weight for SRV records.
	Weight *int64 `json:"weight"`
	// client
	c *Client
	// parent
	d *Domain
}

// init
func (r *DomainRecord) init(c *Client) error {
	r.c = c
	return nil
}

// Destroy this record.
func (r *DomainRecord) Destroy() error {
	return r.c.del(join("domains", r.d.Name, "records", r.Id))
}

// Update the record. Updates can take several seconds to actually update
// even after this method returns successfully, so becareful if you do a
// fetch for the record straight after updating.
func (r *DomainRecord) Set(cfg *DomainConfig) error {
	res, err := r.c.put(join("domains", r.d.Name, "records", r.Id), cfg)
	if err != nil {
		return err
	}
	b := res.Record
	r.Type = b.Type
	r.Name = b.Name
	r.Data = b.Data
	r.Priority = b.Priority
	r.Port = b.Port
	r.Weight = b.Weight
	return nil
}
