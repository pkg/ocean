package ocean

// secret fields
type hiddenSnapshotFields struct {
	RegionSlugs []string `json:"regions,omitempty"`
}

// Snapshot hold Snapshot data
type Snapshot struct {
	// A unique number used to identify and reference a specific image.
	Id int64 `json:"id,omitempty" pk:"true"`
	// The display name of the image
	Name string `json:"name,omitempty"`
	// The base distribution used for this image.
	Distribution string `json:"distribution,omitempty"`
	// A uniquely identifying string that is associated with each of the DigitalOcean-provided public images.
	Slug string `json:"slug,omitempty"`
	// An image that is public is available to all accounts. A non-public image is only accessible from your account.
	Public bool `json:"public,omitempty"`
	// hidden
	hiddenSnapshotFields
	// client
	c *Client
}

// init thing
func (s *Snapshot) init(c *Client) error {
	s.c = c
	return nil
}

// Regions the snapshot is available in
func (s *Snapshot) Regions() ([]*Region, error) {
	return s.c.slugsToRegions(s.RegionSlugs)
}
