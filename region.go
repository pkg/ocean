package ocean

import (
	"encoding/json"
)

// hidden from public api
type hiddenRegionFields struct {
	SizeSlugs []string `json:"sizes,omitempty"`
}

// Region hold Region data
type Region struct {
	// The display name of the region. This will be a full name that is used in the control panel and other interfaces.
	Name string `json:"name,omitempty"`
	// A human-readable string that is used as a unique identifier for each region.
	Slug string `json:"slug,omitempty" pk:"true"`
	// This is a boolean value that represents whether new Droplets can be created in this region.
	Available bool `json:"available,omitempty"`
	// This attribute is set to an array which contains features available in this region
	Features []string `json:"features,omitempty"`
	// hidden
	hiddenRegionFields
	// Client
	c *Client
}

// init
func (r *Region) init(c *Client) error {
	r.c = c
	return nil
}

// Marshal *Region as a 'slug'
func (r *Region) MarshalJSON() (data []byte, err error) {
	return json.Marshal(r.Slug)
}

// Return sizes available in this region
func (r *Region) Sizes() ([]*Size, error) {
	return r.c.slugsToSizes(r.SizeSlugs)
}

// Fetch all available regions.
func (c *Client) Regions() ([]*Region, error) {
	c.regionsLock.Lock()
	defer c.regionsLock.Unlock()
	if c.cachedRegions != nil {
		return c.cachedRegions, nil
	}
	res, err := c.get("regions")
	if err != nil {
		return nil, err
	}
	if c.EnableCache {
		c.cachedRegions = res.Regions
	}
	return res.Regions, nil
}

// Fetch a region by it's slug
func (c *Client) Region(slug string) (*Region, error) {
	regions, err := c.Regions()
	if err != nil {
		return nil, err
	}
	for _, r := range regions {
		if r.Slug == slug {
			return r, nil
		}
	}
	return nil, ErrNotFound
}

func (c *Client) slugsToRegions(slugs []string) (vs []*Region, err error) {
	vs = make([]*Region, len(slugs))
	for i, s := range slugs {
		vs[i], err = c.Region(s)
		if err != nil {
			return
		}
	}
	return
}
