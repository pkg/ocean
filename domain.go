package ocean

// Domain hold domain data
type Domain struct {
	// The name of the domain itself. This should follow the standard domain format of domain.TLD. For instance, example.com is a valid domain name.
	Name string `json:"name,omitempty" pk:"true"`
	// This value is the time to live for the records on this domain, in seconds. This defines the time frame that clients can cache queried information before a refresh should be requested.
	Ttl int64 `json:"ttl,omitempty"`
	// This attribute contains the complete contents of the zone file for the selected domain. Individual domain record resources should be used to get more granular control over records. However, this attribute can also be used to get information about the SOA record, which is created automatically and is not accessible as an individual record resource.
	ZoneFile string `json:"zone_file,omitempty"`
	// private
	c *Client
}

// init thing
func (d *Domain) init(c *Client) error {
	d.c = c
	return nil
}

// Delete the domain
func (d *Domain) Destroy() error {
	return d.c.del(join("domains", d.Name))
}

// Fetch records for the domain
func (d *Domain) Records() ([]*DomainRecord, error) {
	res, err := d.c.get(join("domains", d.Name, "records"))
	if err != nil {
		return nil, err
	}
	for _, r := range res.Records {
		r.d = d
	}
	return res.Records, nil
}

// Fetch a record for the domain by id
func (d *Domain) Record(id int64) (*DomainRecord, error) {
	res, err := d.c.get(join("domains", d.Name, "records", id))
	if err != nil {
		return nil, err
	}
	res.Record.d = d
	return res.Record, err
}

// Create a new DNS record.
func (d *Domain) NewRecord(cfg *DomainConfig) (*DomainRecord, error) {
	res, err := d.c.post(join("domains", d.Name, "records"), cfg)
	if err != nil {
		return nil, err
	}
	res.Record.d = d
	return res.Record, err
}

// Create a domain for the given ipAddress.
// NOTE: The returned *Domain will have an empty zone-file as it takes a
// while before this is generated/propagated on digital ocean.
func (c *Client) NewDomain(name string, ip string) (d *Domain, err error) {
	req := struct {
		Name string `json:"name"`
		Ip   string `json:"ip_address"`
	}{
		Name: name,
		Ip:   ip,
	}
	res, err := c.post("domains", req)
	return res.Domain, err
}

// Fetch domains
func (c *Client) Domains() ([]*Domain, error) {
	res, err := c.get("domains")
	return res.Domains, err
}

// Fetch domain by name
func (c *Client) Domain(name string) (*Domain, error) {
	res, err := c.get(join("domains", name))
	return res.Domain, err
}
