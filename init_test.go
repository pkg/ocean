package ocean

import (
	"fmt"
	"log"
	"os"
)

var client *Client

func init() {
	key := os.ExpandEnv("$DOKEY")
	if key == "" {
		log.Fatal("You must specify a valid digital ocean API key by setting the DOKEY environment variable to run tests")
	}
	client = NewClient(key)
	client.PerPage = 100
	if os.ExpandEnv("$DEBUG") == "1" {
		client.Logger = log.New(os.Stdout, "", log.Lshortfile)
	}
	if n, err := client.RequestLimit(); err == nil {
		fmt.Println("limit", n)
	}
	if n, err := client.RequestsRemaining(); err == nil {
		fmt.Println("remaining", n)
	}
	if t, err := client.RequestLimitResetAt(); err == nil {
		fmt.Println("limit reset's at", t)
	}
}
