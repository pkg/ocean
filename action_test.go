package ocean

import (
	"testing"
)

func TestGetActions(t *testing.T) {
	actions, err := client.Actions()
	if err != nil {
		t.Fatal(err)
	}
	for _, a := range actions {
		switch a.Status {
		case InProgress:
		case Errored:
		case Completed:
		default:
			t.Fatalf("expected action status to be of a known type but got %s", a.Status)
		}
	}
}
