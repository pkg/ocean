package ocean

import (
	"encoding/json"
	"fmt"
)

// SSH Keys
type Key struct {
	// This is a unique identification number for the key.
	Id int64 `json:"id" pk:"true"`
	// This is the human-readable display name for the given SSH key.
	Name string `json:"name"`
	// This attribute contains the fingerprint value that is generated from the public key.
	Fingerprint string `json:"fingerprint"`
	// This attribute contains the entire public key string. This is what is embedded into the root user's authorized_keys file.
	PublicKey string `json:"public_key"`
	// parent client
	c *Client
}

// init
func (k *Key) init(c *Client) error {
	k.c = c
	return nil
}

// Delete this SSH Key
func (k *Key) Destroy() error {
	return k.c.del(join("account", "keys", k.Id))
}

// Update the name of the key
func (k *Key) SetName(name string) error {
	req := struct {
		Name string `json:"name"`
	}{
		Name: name,
	}
	res, err := k.c.put(join("account", "keys", k.Id), req)
	if err != nil {
		return err
	}
	k.Name = res.Key.Name
	return nil
}

// Marshal *Key as a Id
func (k *Key) MarshalJSON() (data []byte, err error) {
	if k.Id == 0 {
		return nil, fmt.Errorf("failed to marshal key to json: key did not have an Id set")
	}
	return json.Marshal(k.Id)
}

// Add a new public ssh key
func (c *Client) NewKey(name string, pubkey string) (*Key, error) {
	req := struct {
		Name      string `json:"name"`
		PublicKey string `json:"public_key"`
	}{
		Name:      name,
		PublicKey: pubkey,
	}
	res, err := c.post(join("account", "keys"), req)
	return res.Key, err
}

// Fetch all keys
func (c *Client) Keys() ([]*Key, error) {
	res, err := c.get(join("account", "keys"))
	return res.Keys, err
}

func (c *Client) Key(idOrFingerprint string) (*Key, error) {
	res, err := c.get(join("account", "keys", idOrFingerprint))
	return res.Key, err
}
