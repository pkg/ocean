package ocean

import (
	"encoding/json"
	"time"
)

type hiddenImageFields struct {
	RegionSlugs []string `json:"regions,omitempty"`
	MinSizeSlug string   `json:"min_size,omitempty"`
}

// Image hold Image data
type Image struct {
	// A unique number that can be used to identify and reference a specific image.
	Id int64 `json:"id,omitempty" pk:"true"`
	// The display name that has been given to an image.
	Name string `json:"name,omitempty"`
	// This attribute describes the base distribution used for this image.
	Distribution string `json:"distribution,omitempty"`
	// A uniquely identifying string that is associated with each of the DigitalOcean-provided public images.
	Slug string `json:"slug,omitempty" pk:"true"`
	// An image that is public is available to all accounts. A non-public image is only accessible from your account.
	Public bool `json:"public,omitempty"`
	// Image creation time
	CreatedAt time.Time `json:"created_at,omitempty"`
	// Required minimum droplet size
	MinSize *Size `json:"-"`
	// hidden
	hiddenImageFields
	// parent
	c *Client
}

// init
func (img *Image) init(c *Client) error {
	img.c = c
	img.MinSize, _ = c.Size(img.MinSizeSlug)
	return nil
}

// Marshal *Size as a Id
func (img *Image) MarshalJSON() (data []byte, err error) {
	return json.Marshal(img.Id)
}

// Return list of actions
func (img *Image) Actions() ([]*Action, error) {
	res, err := img.c.get(join("images", img.Id, "actions"))
	return res.Actions, err
}

// Regions image is available in.
func (img *Image) Regions() ([]*Region, error) {
	return img.c.slugsToRegions(img.RegionSlugs)
}

// Execute an action on a Droplet.
func (img *Image) execute(cmd interface{}) (*Action, error) {
	res, err := img.c.post(join("images", img.Id, "actions"), cmd)
	return res.Action, err
}

// Disable backups on an existing Droplet.
func (img *Image) Transfer(r *Region) (*Action, error) {
	return img.execute(&struct {
		Type   string `json:"type"`
		Region string `json:"region"`
	}{
		Type:   "transfer",
		Region: r.Slug,
	})
}

// Fetch all available distrubtion images.
func (c *Client) Images() ([]*Image, error) {
	if c.cachedImages != nil {
		return c.cachedImages, nil
	}
	res, err := c.get(join("images"), "type=distribution")
	if err != nil {
		return nil, err
	}
	c.cachedImages = res.Images
	return res.Images, err
}

// Fetch image by Slug
func (c *Client) Image(slug string) (*Image, error) {
	imgs, err := c.Images()
	if err != nil {
		return nil, err
	}
	for _, img := range imgs {
		if img.Slug == slug {
			return img, nil
		}
	}
	return nil, ErrNotFound
}
