package ocean

import (
	"encoding/json"
	"fmt"
)

// hidden fields
type hiddenBackupFields struct {
	RegionSlugs []string `json:"regions,omitempty"`
}

// Backup hold Backup data
type Backup struct {
	// A unique number that can be used to identify and reference a specific image.
	Id int64 `json:"id,omitempty" pk:"true"`
	// The display name that has been given to an image.
	Name string `json:"name,omitempty"`
	// This attribute describes the base distribution used for this image.
	Distribution string `json:"distribution,omitempty"`
	// A uniquely identifying string that is associated with each of the DigitalOcean-provided public images.
	Slug string `json:"slug,omitempty"`
	// An image that is public is available to all accounts. A non-public image is only accessible from your account.
	Public bool `json:"public,omitempty"`
	// hidden
	hiddenBackupFields
	// Client
	c *Client
}

// init thing
func (r *Backup) init(c *Client) error {
	r.c = c
	return nil
}

// Return regions the image is available in.
func (r *Backup) Regions() ([]*Region, error) {
	return r.c.slugsToRegions(r.RegionSlugs)
}

// Marshal *Backup as a Id
func (r *Backup) MarshalJSON() (data []byte, err error) {
	if r.Id == 0 {
		return nil, fmt.Errorf("failed to marshal backup value to json: backup did not have an Id set")
	}
	return json.Marshal(r.Id)
}
