package ocean

import (
	"time"
)

const (
	// Status for pending actions
	InProgress = "in-progress"
	// Status for finished actions
	Completed = "completed"
	// Status for failed actions
	Errored = "errored"
)

// secret exported fields
type hiddenActionFields struct {
	RegionSlug string `json:"region,omitempty"`
}

// Actions
type Action struct {
	// A unique numeric ID that can be used to identify and reference an action.
	Id int64 `json:"id,omitempty" pk:"true"`
	// The current status of the action. Use constants InProgress, Completed and Errored
	Status string `json:"status,omitempty"`
	// This is the type of action that the object represents.
	Type string `json:"type,omitempty"`
	// time that represents when the action was initiated.
	StartedAt *time.Time `json:"started_at,omitempty"`
	// time that represents when the action was  completed.
	CompletedAt *time.Time `json:"completed_at,omitempty"`
	// A unique identifier for the resource that the action is associated with.
	ResourceId int64 `json:"resource_id,omitempty"`
	// The type of resource that the action is associated with.
	ResourceType string `json:"resource_type,omitempty"`
	// The region where the action occured
	Region *Region `json:"-"`
	// client
	c *Client
	hiddenActionFields
	done chan bool
	err  error
}

// init
func (a *Action) init(c *Client) (err error) {
	a.c = c
	a.Region, err = c.Region(a.RegionSlug)
	return
}

// Wait will block while the action's status is InProgress.
// or until an error is returned
func (a *Action) Wait() error {
	if a.Status != InProgress {
		return nil
	}
	a.poll()
	<-a.done
	return a.err
}

func (a *Action) poll() {
	if a.done != nil {
		return
	}
	a.err = nil
	a.done = make(chan bool)
	go func() {
		var err error
		for {
			err = a.refresh()
			if err != nil {
				break
			}
			if a.Status != InProgress {
				break
			}
			time.Sleep(a.c.PollDuration)
		}
		a.err = err
		close(a.done)
		a.done = nil
	}()
}

// update the record
func (a *Action) refresh() error {
	res, err := a.c.get(join("actions", a.Id))
	if err != nil {
		return err
	}
	b := res.Action
	a.Id = b.Id
	a.Status = b.Status
	a.Type = b.Type
	a.StartedAt = b.StartedAt
	a.CompletedAt = b.CompletedAt
	a.ResourceId = b.ResourceId
	a.ResourceType = b.ResourceType
	a.RegionSlug = b.RegionSlug
	return a.init(a.c)
}

// Returns a list of all actions.
func (c *Client) Actions() ([]*Action, error) {
	res, err := c.get("actions")
	return res.Actions, err
}

// Returns the action for the given id
func (c *Client) Action(id int64) (*Action, error) {
	res, err := c.get(join("actions", id))
	return res.Action, err
}
