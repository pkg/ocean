package ocean

import (
	"time"
)

const (
	New     = "new"
	Active  = "active"
	Off     = "off"
	Archive = "archive"
)

type Network struct {
	IpAddress string `json:"ip_address,omitempty"`
	Netmask   string `json:"netmask,omitempty"`
	Gateway   string `json:"gateway,omitempty"`
	Type      string `json:"type,omitempty"`
}

type Networks struct {
	V4 []*Network `json:"v4,omitempty"`
	V6 []*Network `json:"v6,omitempty"`
}

// DropletConfig is used when creating new Droplets
type DropletConfig struct {
	// The human-readable name set for the Droplet instance.
	Name string `json:"name,omitempty"`
	// The region to deploy to.
	Region *Region `json:"region,omitempty"`
	// The size of the droplet. (Use client.Sizes() to fetch available sizes)
	Size *Size `json:"size,omitempty"`
	// The base image for the new droplet. (Use client.Images() to fetch available images)
	Image *Image `json:"image,omitempty"`
	// Enable backups for the droplet.
	Backups bool `json:"backups,omitempty"`
	// An array containing the keys that you wish to embed in the Droplet's root account upon creation. (Use client.Keys() to fetch available keys)
	Keys []*Key `json:"ssh_keys,omitempty"`
	// A boolean indicating whether IPv6 is enabled on the Droplet.
	Ipv6 bool `json:"ipv6,omitempty"`
	// A boolean indicating whether private networking is enabled for the Droplet. Private networking is currently only available in certain regions.
	PrivateNetworking bool `json:"private_networking,omitempty"`
	// A string of the desired User Data for the Droplet. User Data is currently only available in regions with metadata listed in their features.
	UserData string `json:"user_data,omitempty"`
}

// internal but exported fields
type hiddenDropletFields struct {
	SizeSlug    string   `json:"size_slug"`
	SnapshotIds []string `json:"snapshot_ids"`
	BackupIds   []string `json:"backup_ids"`
}

// Droplet hold Droplet data
type Droplet struct {
	// A unique identifier for each Droplet instance. This is automatically generated upon Droplet creation.
	Id int64 `json:"id,omitempty" pk:true"`
	// The human-readable name set for the Droplet instance.
	Name string `json:"name,omitempty"`
	// Memory of the Droplet in megabytes.
	Memory int64 `json:"memory,omitempty"`
	// The number of virtual CPUs.
	Vcpus int64 `json:"vcpus,omitempty"`
	// The size of the Droplet's disk in gigabytes.
	Disk int64 `json:"disk,omitempty"`
	// A boolean value indicating whether the Droplet has been locked, preventing actions by users.
	Locked bool `json:"locked,omitempty"`
	// A time value given in ISO8601 combined date and time format that represents when the Droplet was created.
	CreatedAt time.Time `json:"created_at,omitempty"`
	// A status string indicating the state of the Droplet instance. This may be "new", "active", "off", or "archive".
	Status string `json:"status,omitempty"`
	// An array of features enabled on this Droplet.
	Features []string `json:"features,omitempty"`
	// The region that the Droplet instance is deployed in.
	Region *Region `json:"region,omitempty"`
	// The base image used to create the Droplet instance.
	Image *Image `json:"image,omitempty"`
	// Details about the system hardware and costs
	Size *Size `json:"size,omitempty"`
	// The details of the network that are configured for the Droplet instance.
	Networks *Networks `json:"networks,omitempty"`
	// The current kernel.
	Kernel *Kernel `json:"networks,omitempty"`
	// Parent client
	c *Client
	hiddenDropletFields
}

// init
func (d *Droplet) init(c *Client) error {
	d.c = c
	d.Size, _ = c.Size(d.SizeSlug)
	return nil
}

// Delete this droplet
func (d *Droplet) Destroy() error {
	return d.c.del(join("droplets", d.Id))
}

// Return list of backups
func (d *Droplet) Backups() ([]*Backup, error) {
	if len(d.BackupIds) == 0 {
		return make([]*Backup, 0), nil
	}
	res, err := d.c.get(join("droplets", d.Id, "backups"))
	return res.Backups, err
}

// Return list of actions
func (d *Droplet) Actions() ([]*Action, error) {
	res, err := d.c.get(join("droplets", d.Id, "actions"))
	return res.Actions, err
}

// Kernels retrieves a list of all kernels available to a Dropet
func (d *Droplet) Kernels() ([]*Kernel, error) {
	res, err := d.c.get(join("droplets", d.Id, "kernels"))
	return res.Kernels, err
}

// Return list of snapshots
func (d *Droplet) Snapshots() ([]*Snapshot, error) {
	if len(d.SnapshotIds) == 0 {
		return make([]*Snapshot, 0), nil
	}
	res, err := d.c.get(join("droplets", d.Id, "snapshots"))
	return res.Snapshots, err
}

// Returns the currently running action for the droplet. If there is no pending action
// then nil will be returned.
func (d *Droplet) PendingAction() (*Action, error) {
	actions, err := d.Actions()
	if err != nil {
		return nil, err
	}
	for _, a := range actions {
		if a.Status == InProgress {
			return a, nil
		}
	}
	return nil, nil
}

// Execute an action on a Droplet.
// If there is already a pending actions then this will block until they are complete.
func (d *Droplet) execute(cmd interface{}) (*Action, error) {
	for {
		action, err := d.PendingAction()
		if err != nil {
			return nil, err
		}
		if action != nil {
			err = action.Wait()
			if err != nil {
				return nil, err
			}
			continue
		}
		break
	}
	res, err := d.c.post(join("droplets", d.Id, "actions"), cmd)
	return res.Action, err
}

// Disable backups on an existing Droplet.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) DisableBackups() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "disable_backups",
	})
}

// Reboot a running droplet.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) Reboot() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "reboot",
	})
}

// Power cycle a running droplet. This is like doing PowerOff then PowerOn
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) PowerCycle() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "power_cycle",
	})
}

// Trigger shutdown of a droplet
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) Shutdown() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "shutdown",
	})
}

// Turn off a droplet. This is like pulling the plug!
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) PowerOff() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "power_off",
	})
}

// Turn on a droplet.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) PowerOn() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "power_on",
	})
}

// Restore droplet using the given image.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) Restore(img *Image) (*Action, error) {
	return d.execute(&struct {
		Type  string `json:"type"`
		Image int64  `json:"image"`
	}{
		Type:  "restore",
		Image: img.Id,
	})
}

// Restore droplet using the given image.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) PasswordReset() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "pw_reset",
	})
}

// Resize the droplet.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) Resize(s *Size) (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
		Size string `json:"size"`
	}{
		Type: "resize",
		Size: s.Slug,
	})
}

// Rebuild the droplet
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) Rebuild(img *Image) (*Action, error) {
	return d.execute(&struct {
		Type  string `json:"type"`
		Image int64  `json:"image"`
	}{
		Type:  "rebuild",
		Image: img.Id,
	})
}

// Rename droplet.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) Rename(name string) (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
		Name string `json:"name"`
	}{
		Type: "rename",
		Name: name,
	})
}

// Change Kernel the droplet is using.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) ChangeKernel(k *Kernel) (*Action, error) {
	return d.execute(&struct {
		Type   string `json:"type"`
		Kernel int64  `json:"kernel"`
	}{
		Type:   "change_kernel",
		Kernel: k.Id,
	})
}

// Enable IPv6 networking for the droplet
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) EnableIpv6() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "enable_ipv6",
	})
}

// Enable private networking for the droplet
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) EnablePrivateNetworking() (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
	}{
		Type: "enable_private_networking",
	})
}

// Create a new snapshot of this droplet.
// Method may block if there are already pending actions on the droplet.
func (d *Droplet) NewSnapshot(name string) (*Action, error) {
	return d.execute(&struct {
		Type string `json:"type"`
		Name string `json:"name"`
	}{
		Type: "snapshot",
		Name: "name",
	})
}

// Returns the Droplet associated with id.
func (c *Client) Droplet(id int64) (*Droplet, error) {
	res, err := c.get(join("droplets", id))
	return res.Droplet, err
}

// Returns all existing droplets
func (c *Client) Droplets() ([]*Droplet, error) {
	res, err := c.get("droplets")
	return res.Droplets, err
}

// Create a new droplet
func (c *Client) NewDroplet(cfg *DropletConfig) (*Droplet, error) {
	res, err := c.post("droplets", cfg)
	return res.Droplet, err
}
