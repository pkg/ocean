package ocean

import (
	"testing"
)

func TestImages(t *testing.T) {
	images, err := client.Images()
	if err != nil {
		t.Fatal(err)
	}
	ifound := false
	islug := "ubuntu-14-04-x32"
	for _, v := range images {
		if islug == v.Slug {
			ifound = true
		}
	}
	if len(images) < 10 {
		t.Fatalf("there should be more than 10 images returned got %d", len(images))
	}
	if !ifound {
		t.Fatalf("expected to find the %s image", islug)
	}
}
