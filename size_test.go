package ocean

import (
	"testing"
)

func TestSizes(t *testing.T) {
	sizes, err := client.Sizes()
	if err != nil {
		t.Fatal(err)
	}
	sslug := "512mb"
	sfound := false
	for _, v := range sizes {
		if sslug == v.Slug {
			sfound = true
		}
	}
	if len(sizes) < 5 {
		t.Fatalf("there should be more than 5 sizes returned got %d", len(sizes))
	}
	if !sfound {
		t.Fatalf("expected to find the %s size", sslug)
	}

}
