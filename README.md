# ocean

Digital Ocean [v2 API](https://developers.digitalocean.com/) client package for Go ([golang](http://golang.org)).

## Status

Digital Ocean's v2 endpoint is still in beta so things might break.

## Quick Tour

First you'll need to create a client...

```
import "bitbucket.org/pkg/ocean"

var client = ocean.NewClient("...YOUR API KEY...")
```

Then you can interact with the API...

```
// Get required size.
size, err := client.Size("512mb")
if err != nil {
	t.Fatal(err)
}

// Get a region.
region,err := client.Region("lon1")
if err != nil {
	t.Fatal(err)
}

// Get stock image.
img, err := client.Image("ubuntu-14-04-x32")
if err != nil {
	t.Fatal(err)
}

// Get ssh keys.
keys, err := client.Keys()
if err != nil {
	t.Fatal(err)
}

// Create a new droplet.
d, err := client.NewDroplet(&ocean.DropletConfig{
	Name:              "test",
	Region:            region,
	Size:              size,
	Image:             img,
	Keys:              keys,
	Backups:           false,
	PrivateNetworking: true,
	UserData:          ``,
	Ipv6:              true,
})
if err != nil {
	t.Fatal(err)
}

// Perform a reboot.
task, err := d.Reboot()
if err != nil {
	t.Fatal(err)
}

// Wait for task to complete.
err = task.Wait()
if err != nil {
	t.Fatal(err)
}

// Remove the droplet.
err = d.Destroy()
if err != nil {
	t.Fatal(err)
}
```

## A note on Actions

Use the `Wait()` method on actions to block until the action is complete.

Methods that perform actions (like `droplet.Reboot()`) will block if there are any pending actions.

## Documenation

See [godoc](http://godoc.org/bitbucket.org/pkg/ocean)

## Example

There's an example cli tool in the ./examples dir.
