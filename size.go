package ocean

import (
	"encoding/json"
)

// secret fields
type hiddenSizeFields struct {
	RegionSlugs []string `json:"regions,omitempty"`
}

// Size hold Size data
type Size struct {
	// A human-readable string that is used to uniquely identify each size.
	Slug string `json:"slug,omitempty" pk:"true"`
	// The amount of transfer bandwidth that is available for Droplets created in this size. This only counts traffic on the public interface. The value is given in terabytes.
	Transfer float64 `json:"transfer,omitempty"`
	// This attribute describes the monthly cost of this Droplet size if the Droplet is kept for an entire month. The value is measured in US dollars.
	PriceMonthly float64 `json:"price_monthly,omitempty"`
	// This describes the price of the Droplet size as measured hourly. The value is measured in US dollars.
	PriceHourly float64 `json:"price_hourly,omitempty"`
	// The amount of RAM allocated to Droplets created of this size. The value is represented in megabytes.
	Memory int64 `json:"memory,omitempty"`
	// The number of virtual CPUs allocated to Droplets of this size.
	Vcpus int64 `json:"vcpus,omitempty"`
	// The amount of disk space set aside for Droplets of this size. The value is represented in gigabytes.
	Disk int64 `json:"disk,omitempty"`
	// hidden
	hiddenSizeFields
	// Client
	c *Client
}

// init thing
func (s *Size) init(c *Client) error {
	s.c = c
	return nil
}

// Return regions where this can be created
func (s *Size) Regions() ([]*Region, error) {
	return s.c.slugsToRegions(s.RegionSlugs)
}

// Marshal *Size as a 'slug'
func (s *Size) MarshalJSON() (data []byte, err error) {
	return json.Marshal(s.Slug)
}

// Fetch all available sizes.
// If client.EnableCache is true then results will be cached until client.Reset is called.
func (c *Client) Sizes() ([]*Size, error) {
	c.sizesLock.Lock()
	defer c.sizesLock.Unlock()
	if c.cachedSizes != nil {
		return c.cachedSizes, nil
	}
	res, err := c.get("sizes")
	if err != nil {
		return nil, err
	}
	if c.EnableCache {
		c.cachedSizes = res.Sizes
	}
	return res.Sizes, nil
}

// Fetch Size record by slug name
func (c *Client) Size(slug string) (*Size, error) {
	sizes, err := c.Sizes()
	if err != nil {
		return nil, err
	}
	for _, s := range sizes {
		if s.Slug == slug {
			return s, nil
		}
	}
	return nil, ErrNotFound
}

// return list of sizes for slugs
func (c *Client) slugsToSizes(slugs []string) (vs []*Size, err error) {
	vs = make([]*Size, len(slugs))
	for i, s := range slugs {
		vs[i], err = c.Size(s)
		if err != nil {
			return
		}
	}
	return
}
