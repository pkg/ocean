package ocean

import (
	"reflect"
)

// Append any slice fields from src to slice fields on dst.
func (resp *response) merge(src *response) (err error) {
	var (
		vSrc = reflect.ValueOf(src).Elem()
		vDst = reflect.ValueOf(resp).Elem()
		t    = vSrc.Type()
	)

	for i := 0; i < t.NumField(); i++ {
		var (
			src = vSrc.Field(i)
			dst = vDst.Field(i)
		)
		switch src.Type().Kind() {
		case reflect.Slice:

			if src.IsNil() {
				continue
			}

			if dst.IsNil() {
				dst.Set(src)
			} else {
				dst.Set(reflect.AppendSlice(dst, src))
			}

		case reflect.Ptr:

			if src.IsNil() {
				continue
			}

			dst.Set(src)

		}
	}
	return
}
