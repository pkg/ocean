package ocean

// Kernel hold Kernel data
type Kernel struct {
	// A unique number used to identify and reference a specific kernel.
	Id int64 `json:"id,omitempty" pk:"true"`
	// The display name of the kernel. This is shown in the web UI and is generally a descriptive title for the kernel in question.
	Name string `json:"name,omitempty"`
	// A standard kernel version string representing the version, patch, and release information.
	Version string `json:"version,omitempty"`
}
