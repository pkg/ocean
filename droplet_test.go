package ocean

import (
	"testing"
)

func TestDroplet(t *testing.T) {
	// get small size
	size, err := client.Size("512mb")
	if err != nil {
		t.Fatal(err)
	}

	region, err := client.Region("lon1")
	if err != nil {
		t.Fatal(err)
	}

	// get stock image
	img, err := client.Image("ubuntu-14-04-x32")
	if err != nil {
		t.Fatal(err)
	}

	// get ssh keys
	keys, err := client.Keys()
	if err != nil {
		t.Fatal(err)
	}

	// check if droplet exists
	var d *Droplet
	droplets, err := client.Droplets()
	if err != nil {
		t.Fatal(err)
	}
	for _, droplet := range droplets {
		if droplet.Name == "test" {
			d = droplet
			break
		}
	}

	// create a new droplet
	if d == nil {
		d, err = client.NewDroplet(&DropletConfig{
			Name:              "test",
			Region:            region,
			Size:              size,
			Image:             img,
			Keys:              keys,
			Backups:           false,
			PrivateNetworking: true,
			UserData:          ``,
			Ipv6:              true,
		})
		if err != nil {
			t.Fatal(err)
		}
	}

	// perform a reboot
	task, err := d.Reboot()
	if err != nil {
		t.Fatal(err)
	}

	// wait for task to complete
	err = task.Wait()
	if err != nil {
		t.Fatal(err)
	}

	// remove the droplet
	err = d.Destroy()
	if err != nil {
		t.Fatal(err)
	}

}
