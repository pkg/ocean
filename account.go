package ocean

type Account struct {
	// The total number of droplets the user may have
	DropletLimit int64 `json:"droplet_limit,omitempty"`
	// The email the user has registered for Digital Ocean with
	Email string `json:"email,omitempty"`
	// The universal identifier for this user
	Uuid string `json:"uuid,omitempty"`
	// If true, the user has verified their account via email.
	EmailVerified bool `json:"email_verified,omitempty"`
}

// Fetch the account details
func (c *Client) Account() (acc *Account, err error) {
	c.accountLock.Lock()
	defer c.accountLock.Unlock()
	if c.cachedAccount != nil {
		return c.cachedAccount, nil
	}
	res, err := c.get("account")
	if err != nil {
		return
	}
	if c.EnableCache {
		c.cachedAccount = res.Account
	}
	return res.Account, nil
}
