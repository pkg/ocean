package ocean

import (
	"testing"
	"time"
)

func TestDomains(t *testing.T) {
	domains, err := client.Domains()
	if err != nil {
		t.Fatal(err)
	}
	names := []string{"bob.com", "alice.com"}
	for _, name := range names {
		// create a domain
		_, err := client.NewDomain(name, "192.168.0.1")
		if err != nil {
			t.Fatal(err)
		}
		// fetch domain
		d, err := client.Domain(name)
		if err != nil {
			t.Error(err)
			continue
		}
		if d == nil {
			t.Error("expected to find domain that was just created")
			continue
		}
		if d.Name != name {
			t.Error("Expected %s to be %s", d.Name, name)
			continue
		}
		// create record
		cfg := &DomainConfig{
			Type: "A",
			Name: "sub",
			Data: "192.168.0.1",
		}
		r, err := d.NewRecord(cfg)
		if err != nil {
			t.Error(err)
			continue
		}
		if r.Data != "192.168.0.1" {
			t.Error("expected to set data field of record")
			continue
		}
		// count should be 0
		records, err := d.Records()
		if err != nil {
			t.Error(err)
			continue
		}
		if len(records) == 0 {
			t.Error("expected there to be at least 1 record got %d", len(records))
			continue
		}
		// update record
		cfg.Name = "subsub"
		err = r.Set(cfg)
		if err != nil {
			t.Error(err)
			continue
		}
		// takes a while for change to show up
		time.Sleep(4 * time.Second)
		// get record
		r, err = d.Record(r.Id)
		if err != nil {
			t.Error(err)
			continue
		}
		if r.Name != "subsub" {
			t.Error("expected to have updated data field of record")
			continue
		}
	}
	// fetch all domains
	found := 0
	domains, err = client.Domains()
	if err != nil {
		t.Fatal(err)
	}
	for _, d := range domains {
		for _, name := range names {
			if d.Name == name {
				found++
				// delete it
				err := d.Destroy()
				if err != nil {
					t.Error(err)
				}
				break
			}
		}
	}
	if found != len(names) {
		t.Fatalf("expected to find %d domains in list got %d", len(names), found)
	}
	// count should be 0
	domains, err = client.Domains()
	if err != nil {
		t.Fatal(err)
	}
	if len(domains) != 0 {
		t.Fatalf("expected all domains to have been deleted")
	}

}
